/* eslint-disable no-console */
'use strict';

// Do this as the first thing so that any code reading it knows the right env.
process.env.NODE_ENV = 'production';

// Makes the script crash on unhandled rejections instead of silently
// ignoring them. In the future, promise rejections that are not handled will
// terminate the Node.js process with a non-zero exit code.
process.on('unhandledRejection', err => {
    throw err;
});

// Ensure environment variables are read.
require('../config/env');

const ora = require('ora');
const path = require('path');
const chalk = require('chalk');
const fs = require('fs-extra');
const webpack = require('webpack');
const config = require('../config/webpack.config.prod');
const paths = require('../config/paths');
const clearConsole = require('react-dev-utils/clearConsole');
const formatWebpackMessages = require('react-dev-utils/formatWebpackMessages');
const printHostingInstructions = require('react-dev-utils/printHostingInstructions');
const FileSizeReporter = require('react-dev-utils/FileSizeReporter');
const printBuildError = require('react-dev-utils/printBuildError');

const measureFileSizesBeforeBuild =
    FileSizeReporter.measureFileSizesBeforeBuild;
const printFileSizesAfterBuild = FileSizeReporter.printFileSizesAfterBuild;
const useYarn = fs.existsSync(paths.yarnLockFile);

// Check for an interactive shell
const isInteractive = process.stdout.isTTY;

// These sizes are pretty large. We'll warn for bundles exceeding them.
const WARN_AFTER_BUNDLE_GZIP_SIZE = 512 * 1024;
const WARN_AFTER_CHUNK_GZIP_SIZE = 1024 * 1024;

// Clear the console on starting
if (isInteractive) {
    clearConsole();
    console.log();
}

// Start the clock
let startTime = Date.now();

// Start the status spinner in the terminal
const spinner = ora('Starting').start();

// First, read the current file sizes in build directory.
// This lets us display how much they changed later.
spinner.text = 'Measuring previous build file sizes';
measureFileSizesBeforeBuild(paths.appProdBuild)
    .then(previousFileSizes => {
        // Remove all content but keep the directory so that
        // if you're in it, you don't end up in Trash
        spinner.text = 'Removing old build';
        fs.emptyDirSync(paths.appProdBuild);
        fs.emptyDirSync(paths.appDist);
        // Copy the public assets over to the build
        spinner.text = 'Copying public assets';
        copyPublicFolder();
        // Start the webpack build
        spinner.text = 'Creating an optimized production build';
        return build(previousFileSizes);
    })
    .then(
        ({ stats, previousFileSizes, warnings }) => {
            // Copy the build to be served
            copyBuildFolder();

            if (warnings.length) {
                spinner.warn(chalk.yellow('Compiled with warnings.\n'));
                console.log(warnings.join('\n\n'));
                console.log(
                    '\nSearch for the ' +
                    chalk.underline(chalk.yellow('keywords')) +
                    ' to learn more about each warning.'
                );
                console.log(
                    'To ignore, add ' +
                    chalk.cyan('// eslint-disable-next-line') +
                    ' to the line before.\n'
                );
            } else {
                spinner.succeed(chalk.green(`Compiled successfully in ${(Date.now() - startTime) / 1000} seconds.\n`));
            }

            console.log('File sizes after gzip:\n');
            printFileSizesAfterBuild(
                stats,
                previousFileSizes,
                paths.appProdBuild,
                WARN_AFTER_BUNDLE_GZIP_SIZE,
                WARN_AFTER_CHUNK_GZIP_SIZE
            );
            console.log();

            const appPackage = require(paths.appPackageJson);
            const publicUrl = paths.publicUrl;
            const publicPath = config.output.publicPath;
            const buildFolder = path.relative(process.cwd(), paths.appProdBuild);
            printHostingInstructions(
                appPackage,
                publicUrl,
                publicPath,
                buildFolder,
                useYarn
            );
        },
        err => {
            spinner.fail(chalk.red('Failed to compile.\n'));
            printBuildError(err);
            process.kill(process.pid); // using `process.exit(1)` does not actually exit
        }
    );

// Create the production build and print the deployment instructions.
function build(previousFileSizes) {
    let compiler = webpack(config);
    return new Promise((resolve, reject) => {
        compiler.run((err, stats) => {
            if (err) {
                return reject(err);
            }
            const messages = formatWebpackMessages(stats.toJson({}, true));
            if (messages.errors.length) {
                // Only keep the first error. Others are often indicative
                // of the same problem, but confuse the reader with noise.
                if (messages.errors.length > 1) {
                    messages.errors.length = 1;
                }
                return reject(new Error(messages.errors.join('\n\n')));
            }
            if (
                process.env.CI && (
                    typeof process.env.CI !== 'string' ||
                    process.env.CI.toLowerCase() !== 'false'
                ) && messages.warnings.length
            ) {
                console.log(
                    chalk.yellow(
                        '\nTreating warnings as errors because process.env.CI = true.\n' +
                        'Most CI servers set it automatically.\n'
                    )
                );
                return reject(new Error(messages.warnings.join('\n\n')));
            }
            return resolve({
                stats,
                previousFileSizes,
                warnings: messages.warnings
            });
        });
    });
}

function copyPublicFolder() {
    fs.copySync(paths.appPublic, paths.appProdBuild, {
        dereference: true,
        filter: file => !file.match(/\.keepme$/)
    });
}

function copyBuildFolder() {
    // keeping separate dev & prod build folders allows webpack to
    // compare the file sizes from the last build, even when
    // we have run a dev build in between.
    fs.copySync(paths.appProdBuild, paths.appDist, {
        dereference: true
    });
}
