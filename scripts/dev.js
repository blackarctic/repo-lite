/* eslint-disable no-console */
'use strict';

// Do this as the first thing so that any code reading it knows the right env.
process.env.NODE_ENV = 'development';

// Makes the script crash on unhandled rejections instead of silently
// ignoring them. In the future, promise rejections that are not handled will
// terminate the Node.js process with a non-zero exit code.
process.on('unhandledRejection', err => {
    throw err;
});

// Ensure environment variables are read.
require('../config/env');

const ora = require('ora');
const chalk = require('chalk');
const fs = require('fs-extra');
const webpack = require('webpack');
const config = require('../config/webpack.config.dev');
const paths = require('../config/paths');
const clearConsole = require('react-dev-utils/clearConsole');
const formatWebpackMessages = require('react-dev-utils/formatWebpackMessages');
const md5File = require('md5-file');

// Create a status spinner for the console
const spinner = ora();

// Check for an interactive shell
const isInteractive = process.stdout.isTTY;

// Disable spinner is serving in the same terminal window
const isServing = process.env.SERVING === 'true';

// Clear the console on starting
if (isInteractive && !isServing) {
    clearConsole();
}
console.log();

// Start the clock
let startTime = Date.now();

// Start the status spinner in the terminal
spinner.start('Starting');

// Remove all content but keep the directory so that
// if you're in it, you don't end up in Trash
spinner.text = 'Removing old build';
fs.emptyDirSync(paths.appDevBuild);
fs.emptyDirSync(paths.appDist);

// Copy the public assets over to the build
spinner.text = 'Copying public assets';
copyPublicFolder();

// Start the webpack build
spinner.text = 'Creating a development build';
build();

// Create the development build
function build() {
    let compiler = webpack(config);

    // Start the compiler and watch
    compiler.watch({
        ignored: /node_modules/
    }, (err, stats) => {
        if (err && err.message) {
            spinner.stop();
            console.log();
            console.log(err.message);
            process.exit(1);
        }
        if (isInteractive && !isServing) {
            clearConsole();
            console.log();
        }
        const messages = formatWebpackMessages(stats.toJson({}, true));
        if (messages.errors.length) {
            // Only keep the first error. Others are often indicative
            // of the same problem, but confuse the reader with noise.
            if (messages.errors.length > 1) {
                messages.errors.length = 1;
            }
            spinner.fail(chalk.red('Build failed\n\n'));
            console.log(chalk.red(messages.errors.join('\n\n')));
        }
        else {
            // Copy the build to be served
            copyBuildFolder();

            if (messages.warnings.length) {
                spinner.warn(chalk.yellow('Build has warnings\n\n'));
                console.log(chalk.yellow(messages.warnings.join('\n\n')));
            } else {
                spinner.succeed(chalk.green(`Compiled successfully in ${(Date.now() - startTime) / 1000} seconds\n\n`));
            }
        }
    });

    // Display feedback to the terminal when building
    compiler.plugin('watch-run', (compilation, callback) => {
        startTime = Date.now();
        if (isInteractive && !isServing) {
            clearConsole();
        }
        console.log();
        spinner.start('Building');
        callback();
    });
}

function copyPublicFolder() {
    fs.copySync(paths.appPublic, paths.appDevBuild, {
        dereference: true,
        filter: file => !file.match(/\.keepme$/)
    });
}

function copyBuildFolder() {
    // keeping separate dev & prod build folders allows webpack to
    // compare the file sizes from the last build, even when
    // we have run a dev build in between.
    fs.copySync(paths.appDevBuild, paths.appDist, {
        dereference: true,
        filter: (src, dest) => {
            // Filter only the changed files to be copied over.
            // This allows for more accurate watching of paths.appDist
            try {
                const destHash = md5File.sync(dest);
                const srcHash = md5File.sync(src);

                return srcHash !== destHash;
            }
            catch (e) {
                // We will get here when dest file does not exist OR when src/dest is a directory.
                // In both cases, we want to allow the copy through.
                return true;
            }
        }
    });
}
