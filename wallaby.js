module.exports = function (wallaby) {

    const testPathExps = [
        'src/**/__tests__/**/*.{ts,tsx,js}',
        'src/**/_tests/**/*.{ts,tsx,js}',
        'src/**/tests/**/*.{ts,tsx,js}',
        'src/**/test/**/*.{ts,tsx,js}',
        'src/**/?(*.)(spec|test|tests).{ts,tsx,js}'
    ];

    return {
        files: [
            'tsconfig.json',
            'config/**/*.js',
            'src/**/*.+(js|jsx|ts|tsx|json|snap|css|less|sass|scss|jpg|jpeg|gif|png|svg)',
            ...testPathExps.map(x => `!${x}`)
        ],

        filesWithNoCoverageCalculated: [
            'tsconfig.json',
            'config/**/*.js',
            'src/**/*.+(json|snap|css|less|sass|scss|jpg|jpeg|gif|png|svg)',

            'src/**/*.d.ts',
            'src/**/mocks/**',
            'src/**/_mocks/**',
            'src/**/__mocks__/**'
        ],

        tests: testPathExps,

        env: {
            type: 'node',
            runner: 'node'
        },

        testFramework: 'jest',

        compilers: {
            '**/*.ts?(x)': wallaby.compilers.typeScript({
              module: 'commonjs'
            })
        },

        preprocessors: {
            '**/*.js?(x)': file => require('babel-core').transform(
              file.content,
              {sourceMap: true, filename: file.path, presets: ['babel-preset-jest']})
        }
    };
};
