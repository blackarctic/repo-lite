/* eslint-disable no-console */
'use strict';

const autoprefixer = require('autoprefixer');
const path = require('path');
const webpack = require('webpack');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const CaseSensitivePathsPlugin = require('case-sensitive-paths-webpack-plugin');
const WatchMissingNodeModulesPlugin = require('react-dev-utils/WatchMissingNodeModulesPlugin');
const ManifestPlugin = require('webpack-manifest-plugin');
const pages = require('./pages');
const paths = require('./paths');
const getClientEnvironment = require('./env');
const TsConfigPathsPlugin = require('tsconfig-paths-webpack-plugin');

// Webpack uses `publicPath` to determine where the app is being served from.
// It requires a trailing slash, or the file assets will get an incorrect path.
const publicPath = paths.servedPath;

// Some apps do not use client-side routing with pushState.
// For these, "homepage" can be set to "." to enable relative asset paths.
const shouldUseRelativeAssetPaths = publicPath === './';

// Source maps are resource heavy and can cause out of memory issue for large source files.
const shouldUseSourceMap = process.env.GENERATE_SOURCEMAP !== 'false';
// `publicUrl` is just like `publicPath`, but we will provide it to our app
// as %PUBLIC_URL% in `index.html` and `process.env.PUBLIC_URL` in JavaScript.
// Omit trailing slash as %PUBLIC_URL%/xyz looks better than %PUBLIC_URL%xyz.
const publicUrl = publicPath.slice(0, -1);
// Get environment variables to inject into our app.
const env = getClientEnvironment(publicUrl);
// Use tsconfig.proto.json to exclude tests and mocks when building and linting
const useTsConfigProto = process.env.USE_TSCONFIG_PROTO === 'true';
const tsConfigPath = useTsConfigProto ? paths.tsConfigProto : paths.tsConfig;

// Note: defined here because it will be used more than once.
const cssFilename = 'static/css/[name].css';

// ExtractTextPlugin expects the build output to be flat.
// (See https://github.com/webpack-contrib/extract-text-webpack-plugin/issues/27)
// However, our output is structured with css, js and media folders.
// To have this structure working with relative paths, we have to use custom options.
const extractTextPluginOptions = shouldUseRelativeAssetPaths
    ? // Making sure that the publicPath goes back to to build folder.
    { publicPath: Array(cssFilename.split('/').length).join('../') }
    : {};

// Configure page entry points
const pageEntryPoints = {};
pages.forEach((page) => {
    pageEntryPoints[page.bundle] = path.join(paths.appPages, page.src, 'index.ts');
});

// This is the development configuration.
// It is focused on developer experience and fast rebuilds.
// The production configuration is different and lives in a separate file.
module.exports = {
    // Don't attempt to continue if there are any errors.
    bail: true,

    // You may want 'eval' instead if you prefer to see the compiled output in DevTools.
    // See the discussion in https://github.com/facebookincubator/create-react-app/issues/343.
    devtool: shouldUseSourceMap ? 'source-map' : false,
    // These are the "entry points" to our application.
    // This means they will be the "root" imports that are included in JS bundle.
    entry: pageEntryPoints,
    output: {
        pathinfo: true,

        // The build folder.
        path: paths.appDevBuild,

        // Generated JS file names (with nested folders).
        // There will be one main bundle, and one file per asynchronous chunk.
        // We don't currently advertise code splitting but Webpack supports it.
        filename: 'static/js/[name].js',
        chunkFilename: 'static/js/[name].chunk.js',

        // We inferred the "public path" (such as / or /my-project) from homepage.
        publicPath: publicPath,

        // Point sourcemap entries to original disk location (format as URL on Windows)
        devtoolModuleFilenameTemplate: info =>
            path
                .relative(paths.appSrc, info.absoluteResourcePath)
                .replace(/\\/g, '/')
    },
    resolve: {
        // This allows you to set a fallback for where Webpack should look for modules.
        // We placed these paths second because we want `node_modules` to "win"
        // if there are any conflicts. This matches Node resolution mechanism.
        // https://github.com/facebookincubator/create-react-app/issues/253
        modules: ['node_modules', paths.appNodeModules].concat(
            // It is guaranteed to exist because we tweak it in `env.js`
            process.env.NODE_PATH.split(path.delimiter).filter(Boolean)
        ),
        // These are the reasonable defaults supported by the Node ecosystem.
        extensions: ['.js', '.jsx', '.json', '.ts', '.tsx'],
        plugins: [
            // For path resolution in TypeScript
            new TsConfigPathsPlugin({ configFile: tsConfigPath })
        ]
    },
    module: {
        strictExportPresence: true,
        rules: [
            // Extracts source maps from existing source files
            {
                enforce: 'pre',
                test: /\.(js)/,
                loader: require.resolve('source-map-loader')
            },
            // Strip any production code blocks.
            {
                test: /\.(js|jsx|ts|tsx)$/,
                enforce: 'pre',
                include: paths.appSrc,
                use: [{
                    options: {
                        start: 'prod-block:start',
                        end: 'prod-block:end'
                    },
                    loader: require.resolve('webpack-strip-block')
                }]
            },
            {
                // "oneOf" will traverse all following loaders until one will
                // match the requirements. When no loader matches it will fall
                // back to the "file" loader at the end of the loader list.
                oneOf: [
                    // "url" loader works just like "file" loader but it also embeds
                    // assets smaller than specified size as data URLs to avoid requests.
                    {
                        test: [/\.bmp$/, /\.gif$/, /\.jpe?g$/, /\.png$/],
                        loader: require.resolve('url-loader'),
                        options: {
                            limit: 10000,
                            name: 'static/media/[name].[ext]'
                        }
                    },
                    // Process JS/TS with TypeScript
                    {
                        test: /\.(ts|tsx|js|jsx)$/,
                        loader: require.resolve('ts-loader'),
                        include: [
                            paths.appSrc
                        ],
                        options: {
                            configFile: tsConfigPath,
                            silent: true,
                            // disable type checker as this is
                            // done with the fork-ts-checker-plugin
                            // in the typecheck webpack build
                            transpileOnly: true
                        }
                    },
                    // The notation here is somewhat confusing.
                    // "postcss" loader applies autoprefixer to our CSS.
                    // "css" loader resolves paths in CSS and adds assets as dependencies.
                    // "style" loader normally turns CSS into JS modules injecting <style>,
                    // but unlike in development configuration, we do something different.
                    // `ExtractTextPlugin` first applies the "postcss" and "css" loaders
                    // (second argument), then grabs the result CSS and puts it into a
                    // separate file in our build process. This way we actually ship
                    // a single CSS file in production instead of JS code injecting <style>
                    // tags. If you use code splitting, however, any async bundles will still
                    // use the "style" loader inside the async code so CSS from them won't be
                    // in the main CSS file.
                    {
                        test: /\.(css)$/,
                        include: [
                            path.join(paths.appNodeModules, '@gkernel/ux-components')
                        ],
                        loader: ExtractTextPlugin.extract(
                            Object.assign({
                                fallback: {
                                    loader: require.resolve('style-loader'),
                                    options: {
                                        hmr: false
                                    }
                                },
                                use: [{
                                    loader: require.resolve('css-loader'),
                                    options: {
                                        importLoaders: 2,
                                        minimize: true,
                                        sourceMap: shouldUseSourceMap
                                    }
                                },
                                {
                                    loader: require.resolve('postcss-loader'),
                                    options: {
                                        // Necessary for external CSS imports to work
                                        // https://github.com/facebookincubator/create-react-app/issues/2677
                                        ident: 'postcss',
                                        plugins: () => [
                                            require('postcss-flexbugs-fixes'),
                                            autoprefixer({
                                                browsers: [
                                                    '>1%',
                                                    'last 4 versions',
                                                    'Firefox ESR',
                                                    'not ie < 9' // React doesn't support IE8 anyway
                                                ],
                                                flexbox: 'no-2009'
                                            })
                                        ],
                                        sourceMap: shouldUseSourceMap
                                    }
                                }]
                            },
                            extractTextPluginOptions
                            )
                        )
                        // Note: this won't work without `new ExtractTextPlugin()` in `plugins`.
                    },
                    // "file" loader makes sure assets end up in the `build` folder.
                    // When you `import` an asset, you get its filename.
                    // This loader doesn't use a "test" so it will catch all modules
                    // that fall through the other loaders.
                    {
                        loader: require.resolve('file-loader'),
                        // Exclude `js` files to keep "css" loader working as it injects
                        // it's runtime that would otherwise processed through "file" loader.
                        // Also exclude `html` and `json` extensions so they get processed
                        // by webpacks internal loaders.
                        exclude: [/\.(ts|tsx|js|jsx)$/, /\.html$/, /\.json$/],
                        options: {
                            name: 'static/media/[name].[ext]'
                        }
                    }
                    // ** STOP ** Are you adding a new loader?
                    // Make sure to add the new loader(s) before the "file" loader.
                ]
            }
        ]
    },
    plugins: [
        // Makes some environment variables available to the JS code, for example:
        // if (process.env.NODE_ENV === 'production') { ... }. See `./env.js`.
        // It is absolutely essential that NODE_ENV was set to production here.
        // Otherwise React will be compiled in the very slow development mode.
        new webpack.DefinePlugin(env.stringified),
        // FIXME: the common bundle is causing visual bugs on the tasks page
        // Make a bundle of modules between all descendants, when the modules are
        // common between at least 2 chunks.
        // new webpack.optimize.CommonsChunkPlugin({
        //     name: 'common',
        //     deepChildren: true,
        //     minChunks: 2
        // }),
        // Watcher doesn't work well if you mistype casing in a path so we use
        // a plugin that prints an error when you attempt to do this.
        // See https://github.com/facebookincubator/create-react-app/issues/240
        new CaseSensitivePathsPlugin(),
        // If you require a missing module and then `npm install` it, you still have
        // to restart the development server for Webpack to discover it. This plugin
        // makes the discovery automatic so you don't have to restart.
        // See https://github.com/facebookincubator/create-react-app/issues/186
        new WatchMissingNodeModulesPlugin(paths.appNodeModules),
        // Note: this won't work without ExtractTextPlugin.extract(..) in `loaders`.
        new ExtractTextPlugin({
            filename: cssFilename,
            allChunks: true
        }),
        // Generate a manifest file which contains a mapping of all asset filenames
        // to their corresponding output file so that tools can pick it up without
        // having to parse `index.html`.
        new ManifestPlugin({
            fileName: 'asset-manifest.json'
        })
    ].filter(x => !!x), // filter out any null or falsy values from this plugins list
    externals: {
        angular: 'angular'
    },
    // Some libraries import Node modules but don't use them in the browser.
    // Tell Webpack to provide empty mocks for them so importing them works.
    node: {
        dgram: 'empty',
        fs: 'empty',
        net: 'empty',
        tls: 'empty',
        child_process: 'empty'
    },
    // Turn off performance hints during development because we don't do any
    // splitting or minification in interest of speed. These warnings become
    // cumbersome.
    performance: {
        hints: false
    }
};
