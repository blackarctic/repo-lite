import * as React from 'react';
import { roundIfNumber } from 'shared/services/MathService';
import { data } from './data';
import photo from 'assets/img/photo.jpg';

export const MyPage: React.StatelessComponent = () => (
    <div>
        <h1>Welcome to My Page!</h1>
        <p>Here is a number: {roundIfNumber(19.86)}.</p>
        <p>My name is {data.name} and I am {data.age} years old.</p>
        <img src={photo}/>
    </div>
);
