import 'jest'; // necessary for types

import * as MockD from '../_mocks/data';
jest.doMock('../data', () => MockD);

import * as React from 'react';
import * as enzyme from 'enzyme';
import * as Adapter from 'enzyme-adapter-react-16';
import toJson from 'enzyme-to-json';
import { setupForComponentTesting } from 'shared/services/TestingService';
import { MyPage } from '../page';

setupForComponentTesting(enzyme, Adapter);

const { shallow, mount } = enzyme;

describe('MyPage', () => {
    it(`mounts without error`, () => {
        mount(<MyPage />);
    });
    it(`renders expected markup`, () => {
        const element = shallow(<MyPage />);
        expect(toJson(element)).toMatchSnapshot();
    });
});
