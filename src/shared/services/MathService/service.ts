import _isNumber from 'lodash-es/isNumber';

export function roundIfNumber(val: number) {
    return _isNumber(val) ? Math.round(val) : null;
}

export function roundWithDecimals(val: string | number, decimals: number) {
    return _isNumber(val) ? val.toFixed(decimals) : Number.parseFloat(val).toFixed(decimals);
}
