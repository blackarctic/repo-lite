import * as React from 'react';
import { render } from 'react-dom';

const renderComponent = (
    PageComponent: React.ComponentClass | React.StatelessComponent,
    elementId: string
) => {
    render(
        <PageComponent />,
        document.getElementById(elementId)
    );
};

export function renderPage(
    PageComponent: React.ComponentClass | React.StatelessComponent
) {
    if (PageComponent) {
        renderComponent(PageComponent, 'page-root');
    }
}
