/* tslint:disable no-console */

import 'jest';

declare const process: any;

export function setupForTesting() {
    // Fail tests on error & warn
    console.warn = jest.fn((m) => { throw new Error(m); });
    console.error = jest.fn((m) => {
        if (m instanceof Error) { throw m; }
        throw new Error(m);
    });
    // Fail on unhandled promise rejection
    process.on('unhandledRejection', (e: any) => {
        console.error(e);
    });
}

export function setupForComponentTesting(enzyme: any, Adapter: any) {
    setupForTesting();
    // Configure Enzyme for use with Jest & ES6+
    enzyme.configure({ adapter: new Adapter() });
}
