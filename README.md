# Documentation

## Quick Start

1. Install dependencies

```bash
npm i
```

2. Run unit tests

```bash
npm test
```

3. Build the bundles

```bash
npm run prod
```

## Commands

Run the following commands with `npm run <command>`. The source for these commands can be found in `package.json`.

- `dev` - Do a development build that will rebuild on changes.
- `proto` - Use this to run `dev` without processing mock and test files.
- `prod` - Do a production build.
- `test` - Run unit tests in interactive watch mode.
- `lint` - Use this to lint all `.js` and `.ts` files.
- `typecheck` - Use this to typecheck all `.ts` files.
